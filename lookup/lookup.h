#ifndef LOOKUP_H_
#define LOOKUP_H_

unsigned int lookup(unsigned int x_val, const unsigned int x_axis[], unsigned int N);
unsigned int lookup_round(unsigned int x_val, const unsigned int idx, const unsigned int x_axis[]);
unsigned int lerp(unsigned int x_val, const unsigned int x_axis[], const unsigned int y_axis[], unsigned int N);

#endif /* LOOKUP_H_ */

