#include <stdio.h>
#include <stdlib.h>

#include "lookup.h"

const long profile_map_temp_degc[] = {800, 1066, 1333, 1599, 1866, 2133, 2399, 2665, 2933,
3200, 3333, 3466, 3600, 3733, 3866, 4000, 4133, 4266, 4400, 4533, 4666, 4800,
4949, 5098, 5248, 5397, 5546, 5696, 5845, 5994, 6144, 6293, 6442, 6592, 6741,
6890, 7040, 6080, 5120, 4160, 3200, 2240, 1280};

const unsigned int profile_map_time_sec[] = {0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110,
120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 260, 270,
280, 290, 300, 310, 320, 330, 340, 350, 360, 370, 380, 390, 400, 410, 420};

int main(int argc, char * argv[])
{
  int ii, idx, out, N = sizeof(profile_map_time_sec)/sizeof(unsigned int) - 1;
  
  for(ii=0;ii<profile_map_time_sec[N];ii++)
  {
    idx = lookup(ii,
                 (unsigned int*)(&profile_map_time_sec[0]),
                 sizeof(profile_map_time_sec)/sizeof(unsigned int));
    idx = lookup_round(ii, idx, (unsigned int*)(&profile_map_time_sec[0]));
    out = lerp(ii, (unsigned int*)(&profile_map_time_sec[0]),(unsigned int*)(&profile_map_temp_degc[0]),(unsigned int)N);
    printf("%d,%d\n",ii,out);
  }
  return 0;
}

