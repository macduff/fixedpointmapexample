#include "lookup.h"

unsigned int lookup(unsigned int x_val, unsigned int x_axis[], unsigned int N)
{
  unsigned int m, ii = 0, jj = N-1;
  
  /* endpoint check */
  if( x_val <= x_axis[0] )
  {
    return 0;
  }
  else if( x_val >= x_axis[N-1] )
  {
    return N-1;
  }
  
  while( ii < jj )
  {
    m = (ii+jj)/2;
    if( x_val > x_axis[m] )
    {
      ii = m + 1;
    }
    else
    {
      jj = m;
    }
  }
  return ii;
}

unsigned int lerp(unsigned int x_val, unsigned int x_axis[], unsigned int y_axis[], unsigned int N)
{
  unsigned int
      idx,
      x_diff,
      x_input_diff,
      y_high,
      y_return,
      y_input_diff;
  
  
  idx = lookup(x_val,x_axis,N);
  
  if( 0 == idx )
  {
    y_return = y_axis[0];
  }
  else if (x_val == x_axis[idx-1])
  {
    y_return = y_axis[idx-1];
  }
  else
  {
    /* Determine the difference between the input and the
       lower bounding X value */
    x_input_diff = (x_val - x_axis[idx-1]);
    
    /* Determine the difference between the bounding X values */
    x_diff = x_axis[idx] - x_axis[idx-1];
    y_high = y_axis[idx];
    if (y_high > y_axis[idx-1])
    {
      /* if Slope is positive find the increase for the change
         in input then add it to the y_low value */
      y_input_diff =((unsigned short)((y_high - y_axis[idx-1]) * x_input_diff))/x_diff;
      y_return = (unsigned short) y_axis[idx-1] + y_input_diff;
    }
    else
    {
      /* if Slope is negative find the decrease for the change
         in input then subtract it from the y_low value */
      y_input_diff =((unsigned short)((y_axis[idx-1] - y_high) * x_input_diff))/x_diff;
      y_return = (unsigned short) y_axis[idx-1] - y_input_diff;
    }
  }
  return(y_return);
}

unsigned int lookup_round(unsigned int x_val, unsigned int idx, unsigned int x_axis[])
{
  unsigned int hi, lo;
  
  if( x_val > x_axis[idx] )
  {
    hi = x_val - x_axis[idx];
    lo = x_axis[idx+1] - x_val;
    if( hi > lo )
    {
      return idx;
    }
    else
    {
      return idx+1;
    }
  }
  else if( x_val < x_axis[idx] )
  {
    hi = x_axis[idx] - x_val;
    lo = x_val - x_axis[idx-1];
    if( hi > lo )
    {
      return idx-1;
    }
    else
    {
      return idx;
    }
  }
  else
  {
      return idx;
  }
}

