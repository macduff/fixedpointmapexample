#include "stdio.h"
#include "stdlib.h"

long profile_map_temp_degc[] = {800, 1066, 1333, 1599, 1866, 2133, 2399, 2665, 2933,
3200, 3333, 3466, 3600, 3733, 3866, 4000, 4133, 4266, 4400, 4533, 4666, 4800,
4949, 5098, 5248, 5397, 5546, 5696, 5845, 5994, 6144, 6293, 6442, 6592, 6741,
6890, 7040, 6080, 5120, 4160, 3200, 2240, 1280};

unsigned int profile_map_time_sec[] = {0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110,
120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 260, 270,
280, 290, 300, 310, 320, 330, 340, 350, 360, 370, 380, 390, 400, 410, 420};

int lookup(unsigned int x_val, unsigned int x_axis[], unsigned int N);

int main(int argv, char ** argc)
{
  unsigned int idx;
  int ii, xval;
  
  if( argv == 2 )
  {
    xval = atoi(argc[1]);
    printf(" %s %d ",argc[0], xval);
  }
  else
  {
    xval = 10;
  }
  
  for(ii=0;ii<421;ii++)
  {
    idx = lookup(ii, profile_map_time_sec, sizeof(profile_map_time_sec)/sizeof(unsigned int));
    printf("%d, %ld\n",ii,profile_map_temp_degc[idx]);
  }
//  printf("lookup for %d is %d\n",xval,idx);
  return 0;
}

